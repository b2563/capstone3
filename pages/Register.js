import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import '../Source.css';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const [isActive, setIsActive] = useState(false);


	function registerUser (e) {
		
		e.preventDefault();

		fetch(`http://localhost:4000/users/register`, {
				method: 'POST',
				headers: {
					"Content-Type": "application/json"
				},

				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				if(data) {

					setFirstName("");
					setLastName("");
					setMobileNo("");
					setEmail("");
					setPassword("");
					setConfirmPassword("");

					Swal.fire({
						title: "Registration Successful!",
						icon: "success",
						text: "Welcome to Gad-gets"
					})

					navigate("/login")

				} else {
					Swal.fire({
						title: "Registration Failed",
						icon: "error",
						text: data.message
					})
				}
			})
	}



useEffect(() => {
	if((firstName !== "" &&
		lastName !== "" &&
		mobileNo.length === 11 &&
		email !== "" &&
		password !== "" && confirmPassword !== "")
		&& (password === confirmPassword)) {

		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [firstName, lastName, mobileNo, email, password, confirmPassword])




	return (
		(user.id !== null)
			? <Navigate to="/viewProducts"/>
			: 
			<>
					<Form onSubmit={ (e) => registerUser(e)}>

						<h1 className="text-center mt-5">Register</h1>
							<Form.Group className="mb-3" controlId="firstName">
							  <Form.Label>First Name</Form.Label>
								  <Form.Control 
								  	 type="text"
								  	 value={firstName}
								  	 onChange={(e) => {setFirstName(e.target.value)}}
								  	 placeholder="Enter your First Name" />
							</Form.Group>

						  	<Form.Group className="mb-3" controlId="lastName">
						  	  <Form.Label>Last Name</Form.Label>
							  	  <Form.Control 
							  	  	 type="text"
							  	  	 value={lastName}
							  	  	 onChange={(e) => {setLastName(e.target.value)}}
							  	  	 placeholder="Enter your Last Name" />
						  	</Form.Group>

						  	<Form.Group className="mb-3" controlId="mobileNo">
						  	   <Form.Label>Mobile Number</Form.Label>
						  	  	  <Form.Control 
						  	  	  	 type="text"
						  	  	  	 value={mobileNo}
						  	  	  	 onChange={(e) => {setMobileNo(e.target.value)}}
						  	  	  	 placeholder="09999999999" />
						  	</Form.Group>

						    <Form.Group className="mb-3" controlId="userEmail">
						       <Form.Label>Email address</Form.Label>
							      <Form.Control 
							         type="email"
							         value={email}
							         onChange={(e) => {setEmail(e.target.value)}}
							         placeholder="Enter email" />
							       <Form.Text className="text-muted">
							         We'll never share your email with anyone else.
							       </Form.Text>
						    </Form.Group>

						      <Form.Group className="mb-3" controlId="password">
						        <Form.Label>Password</Form.Label>
							      <Form.Control 
							         type="password" 
							         value={password}
							         onChange={(e) => {setPassword(e.target.value)}}
							         placeholder="Enter Your Password" />
						      </Form.Group>

						      <Form.Group className="mb-3" controlId="confirmPassword">
						        <Form.Label>Verify Password</Form.Label>
							       <Form.Control 
							         type="password"
							         value={confirmPassword}
							         onChange={(e) => {setConfirmPassword(e.target.value)}}
							         placeholder="Verify Your Password" />
						      </Form.Group>

						      	{ 
						      		isActive 
						      		
						      		? <Button variant="primary" type="submit" id="submitBtn">Submit</Button> 	
									: <Button variant="btn btn-outline-primary" type="submit" id="submitBtn" disabled>Submit</Button>    
						     	}
					</Form>
			</>

	
 	);
}



