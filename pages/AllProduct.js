import { useState,useEffect } from 'react';
import ProductCard from '../components/ProductCard'

export default function AllProduct(){

	const [ productDetails, setProductDetails ] = useState([])

	useEffect(() => {
		fetch(`http://localhost:4000/products/all`, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setProductDetails(data.map(product => {
				return (
					<ProductCard key={product._id} prodProp={product}/>
				)
			}))	
		})
		.catch(err=>console.log(err))
},[])

	return (

		<>
			<h1 className="my-5 text-center">Available Products</h1>
			{productDetails}
		</>
		)
}

