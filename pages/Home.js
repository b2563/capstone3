import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
    const data = {
        title: "Gad-gets",
        content: "Gadgets for affordable prices",
        destination: "/products",
        label: "Order Now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}