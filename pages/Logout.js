import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function Logout(){

	const {setUser, unsetUser} = useContext(UserContext);


	unsetUser();

	useEffect(() => {
			setUser({id: null})	
	},[setUser])
	
	Swal.fire({
		title: "You have successfully logged out.",
		icon: "success"
	})

	return(
		<Navigate to='/'/>
	)
}

