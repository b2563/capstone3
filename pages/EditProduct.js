import { Form, Button, Container } from 'react-bootstrap';
import { useNavigate, useParams, Navigate, Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../Source.css';

export default function EditProduct() {

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const[ name, setName ] = useState("");
	const[ description, setDescription ] = useState("");
	const[ price, setPrice ] = useState(0);

	const[ isActive, setIsActive ] = useState(false);

	
	function editProduct(e){

		e.preventDefault()
		console.log(productId)
		fetch(`http://localhost:4000/products/${productId}`, {

			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				Swal.fire({
					icon: "success",
					title: "Product Update Successful.",
					text: `${name} is now updated`
				})	
				navigate(`/admin`);

				} else {

				Swal.fire({
					icon: "error",
					title: "Product Update Failed.",
					text: data.message
				})
			}
		})
		setName('');
		setDescription('');
		setPrice(0);
	};

	useEffect(()=>{

		if(name !== "" && description !== "" && price > 0)
		{
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[name, description, price]);

	useEffect(()=> {
			console.log(productId);
	    	fetch(`http://localhost:4000/products/`)
	    	.then(res => res.json())
	    	.then(data => {

	    		console.log(data)

	    		setName(data.name);
	    		setDescription(data.description);
	    		setPrice(data.price);
	    	});

	    }, [productId]);


	return (
		user.isAdmin
		?
		
		<>
		<Container className="centerAdd">
			<h2 className = "text-center">Update Product</h2>
			<Form onSubmit={(e) => editProduct(e)}>
				<Form.Group controlId="name">
					<Form.Label>Product Name:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Name"
						value={name}
						onChange={e => setName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="description">
					<Form.Label>Description:</Form.Label>
					<Form.Control 
						as="textarea"
						rows={3}
						placeholder="Enter Product Description"
						value={description}
						onChange={e => setDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 
						type="number"
						placeholder="Enter Price"
						value={price}
						onChange={e => setPrice(e.target.value)}
						required
					/>
				</Form.Group>								

				{
					isActive 
					? <Button type="submit" variant="primary" className = "btnCrtProd">Update</Button>
					: <Button type="submit" variant="danger" disabled className = "btnCrtProd">Update</Button>
				}
				<Button as={Link} to="/admin" variant="success" type="submit" className="btnCrtProd mx-2">
					Cancel
				</Button>

			</Form>
			</Container>
		</>
		:
		<Navigate to="/viewProducts"/>
	)
}

