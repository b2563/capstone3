import React,{useState,useEffect, useContext} from 'react';
import { Card, Button, Container} from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import UserContext from '../userContext'
import '../Source.css';


export default function AdminProduct({adminProp}){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { name, description, price, _id } = adminProp;

	return (

		<Container>
			<Card className="p-1 m-2 minCard text-center">
			    <Card.Body>
			    	<Card.Title>{name}</Card.Title>
			    	<Card.Text>{description}</Card.Text>
			   	 	<Card.Text>PHP: {price}</Card.Text>
			   	 	{
			   	 		user.isAdmin === true
			   	 		?
			   	 		<Link className="btn btn-primary" to={`/cart/${_id}`}>Add To Cart</Link>	
			   	 		:
			   	 		<Link className="btn btn-warning btn-block" to="/login">Login to Shop</Link>
			   	 	}
				</Card.Body>
			</Card>
		</Container>
	)
}

