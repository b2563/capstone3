import React,{useState,useEffect,useContext} from 'react';
import {CardGroup, Card, Button, Row, Col, Container} from 'react-bootstrap';
import { Link, useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../userContext'
import '../Source.css';


export default function Product({prodProp}) {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const { name, description, price, _id } = prodProp;

	const [ order, setOrder] = useState(0);
	const [ stocks, setStocks ] = useState(10)

	function orderItem () {
		if(stocks > 0) {
			setOrder(order + 1)
			setStocks(stocks - 1)
		}
	};

	useEffect(() => {
		if (stocks === 0) {
			alert("No more order available.")
		}
	}, [stocks])


	return (

	<Container>
		<Card className=" minCard text-center p-1 m-2">
		    <Card.Body>
		    	<Card.Title>{name}</Card.Title>
		    	<Card.Text>{description}</Card.Text>
		   	 	<Card.Text>PHP: {price }</Card.Text>
		   	 	{ user.id
		   	 	?
		   	 		user.isAdmin
		   	 			? 
		   	 			<>
		   	 				<Navigate to="/admin"/>
		   	 			</>
		   	 			: 
		   	 			<>
		   	 				<Link className="btn btn-primary m-2" size="lg" to={`/cart/${_id}`}>Order</Link>	
		   	 			</>
		   	 	: <Link className="btn btn-warning btn-block" size="lg" to="/login">Login to Shop</Link>
		   	 	}
			</Card.Body>
		</Card>
	</Container>

	)
}

